-- 21 
select codcoche from distribuyen 
join concesionario 
on distribuyen.cifc=concesionario.cifc
where concesionario.ciudad like 'Barcelona';
-- 22 
select dni from venden 
join concesionario 
on venden.cifc=concesionario.cifc
where concesionario.ciudad like 'madrid';
-- 23
select codcoche from venden 
join concesionario 
on venden.cifc=concesionario.cifc
where concesionario.ciudad like 'madrid';
-- 24 
select color from venden 
join concesionario 
on venden.cifc=concesionario.cifc
where concesionario.nombre like 'acar';
-- 25 
select cliente.nombe from cliente 
join venden 
on venden.dni=cliente.dni
join concesionario 
on venden.cifc=concesionario.cifc
where concesionario.nombre like 'dcar';
-- 26 
select coche.nombre, coche.modelo from coche
join venden
on coche.codcoche=venden.codcoche
join concesionario
on venden.cifc=concesionario.cifc
where concesionario.ciudad like 'barcelona';
-- 27
select marca.nombre from marca
join fabrica
on fabrica.cifm=marca.cifm
join coche
on fabrica.codcoche=coche.codcoche
where coche.modelo like 'gtd';
-- 28
select coche.codcoche from coche
join venden
on coche.codcoche=venden.codcoche
join cliente
on cliente.dni=venden.dni
join concesionario
on concesionario.cifc=venden.cifc
where cliente.ciudad = concesionario.ciudad;
-- 29
select coche.codcoche from coche
join venden
on coche.codcoche=venden.codcoche
join cliente
on cliente.dni=venden.dni
join concesionario
on concesionario.cifc=venden.cifc
where cliente.ciudad not like concesionario.ciudad;
-- 30 
select cliente.nombe, cliente.apellido from cliente
join venden on venden.dni=cliente.dni
join coche on coche.codcoche=venden.codcoche
where coche.modelo like 'glx'
and venden.color like 'blanco';
-- 31 
select coche.codcoche from coche
join venden on venden.codcoche=coche.codcoche
join concesionario on concesionario.cifc=venden.cifc
join cliente on cliente.dni=venden.dni
where cliente.ciudad like 'madrid' and concesionario.ciudad like 'madrid';
-- 32
select cliente.nombe, cliente.apellido from cliente
join venden on venden.dni=cliente.dni
join coche on coche.codcoche=venden.codcoche
join concesionario on concesionario.cifc=venden.cifc
join distribuyen on distribuyen.cifc=concesionario.cifc 
and distribuyen.codcoche=coche.codcoche
where distribuyen.cantidad is not null
and coche.modelo like 'glx';
-- 33
select cliente.nombe, cliente.apellido from cliente
join venden on venden.dni=cliente.dni
join coche on coche.codcoche=venden.codcoche
join concesionario on concesionario.cifc=venden.cifc
join distribuyen on distribuyen.cifc=concesionario.cifc 
and distribuyen.codcoche=coche.codcoche
where distribuyen.cantidad is not null
and coche.modelo like 'glx'
and concesionario.ciudad like 'madrid';
-- 34
select marca.cifm, cliente.dni from marca
join fabrica on marca.cifm=fabrica.cifm
join coche on coche.codcoche=fabrica.codcoche
join venden on coche.codcoche=venden.codcoche
join cliente on cliente.dni= venden.dni
where cliente.ciudad != marca.ciudad;
-- 35
select marca.cifm, cliente.dni from marca
join fabrica on marca.cifm=fabrica.cifm
join coche on coche.codcoche=fabrica.codcoche
join venden on coche.codcoche=venden.codcoche
join cliente on cliente.dni= venden.dni
where cliente.ciudad = marca.ciudad;
-- 36
select cliente.nombe, cliente.apellido from cliente 
where cliente.dni<(select cliente.dni from cliente
where cliente.nombe like 'juan' and cliente.apellido like 'martin');

select cliente.nombe, cliente.apellido from cliente resto
join cliente juan on resto.dni=juan.dni
where juan.nombe like 'juan' and juan.apellido like 'martin';
-- 37
select coche.modelo from coche
join fabrica on fabrica.codcoche=coche.codcoche
join marca on marca.cifm=fabrica.cifm

