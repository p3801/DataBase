-- 1
select cliente.nombre_cliente, empleado.nombre as 'Nombre Empleado', 
empleado.apellido1 as 'Su apellido'from cliente
join empleado on empleado.codigo_empleado=cliente.codigo_empleado_rep_ventas;
-- 2
select distinct cliente.nombre_cliente, empleado.nombre as 'Nombre Empleado' from cliente
join empleado on empleado.codigo_empleado=cliente.codigo_empleado_rep_ventas
join pago on pago.codigo_cliente=cliente.codigo_cliente;
-- 3 
select distinct cliente.nombre_cliente, empleado.nombre as 'Nombre Empleado' from cliente
join empleado on empleado.codigo_empleado=cliente.codigo_empleado_rep_ventas
join pago on pago.codigo_cliente!=cliente.codigo_cliente;
-- 4
select distinct cliente.nombre_cliente, empleado.nombre as 'Nombre Empleado',oficina.ciudad
from cliente
join empleado on empleado.codigo_empleado=cliente.codigo_empleado_rep_ventas
join pago on pago.codigo_cliente=cliente.codigo_cliente
join oficina on empleado.codigo_oficina=oficina.codigo_oficina;
-- 5
select distinct cliente.nombre_cliente, empleado.nombre as 'Nombre Empleado',oficina.ciudad
from cliente
join empleado on empleado.codigo_empleado=cliente.codigo_empleado_rep_ventas
join pago on pago.codigo_cliente!=cliente.codigo_cliente
join oficina on oficina.codigo_oficina=empleado.codigo_oficina;
-- 6
select distinct oficina.linea_direccion1 from oficina
join empleado on empleado.codigo_oficina=oficina.codigo_oficina
join cliente on  empleado.codigo_empleado=cliente.codigo_empleado_rep_ventas
where cliente.ciudad like 'fuenlabrada';
-- 7
select distinct cliente.nombre_cliente, empleado.nombre as 'Nombre Empleado',oficina.ciudad
from cliente
join empleado on empleado.codigo_empleado=cliente.codigo_empleado_rep_ventas
join oficina on empleado.codigo_oficina=oficina.codigo_oficina;
-- 8 
select empleado.nombre, empleado.apellido1 , jefe.nombre, jefe.apellido1 from empleado
join empleado jefe on empleado.codigo_jefe=jefe.codigo_empleado;
-- 9 
select distinct cliente.nombre_cliente from cliente
join pedido on cliente.codigo_cliente=pedido.codigo_cliente
where pedido.fecha_esperada<pedido.fecha_entrega;
-- 10 
select distinct gama_producto.gama from gama_producto
join producto on gama_producto.gama=producto.gama
join detalle_pedido on detalle_pedido.codigo_producto=producto.codigo_producto;

-- consulta resumen
-- 1.¿Cuántos empleados hay en la compañía?
select * from empleado;
select count(codigo_empleado) from empleado;
-- 2.¿Cuántos clientes tiene cada país?
select * from cliente;
select count(codigo_cliente),pais from cliente group by pais;
-- 3.¿Cuál fue el pago medio en 2009?
select * from pago;
select avg(total),year(fecha_pago) from pago where year(fecha_pago) = 2009;
-- 4.¿Cuántos pedidos hay en cada estado? Ordena el resultado de forma descendente por el número de pedidos.
select * from pedido;
select count(codigo_pedido),estado from pedido group by estado order by count(codigo_pedido) desc;
-- 5.Calcula el precio de venta del producto más caro y más barato en una misma consulta.
select * from producto;
select max(precio_venta) "Más Caro",min(precio_venta)"Más Barato" from producto;
-- 6.Calcula el número de clientes que tiene la empresa.
select * from cliente;
select count(codigo_cliente) from cliente;
-- 7.¿Cuántos clientes tiene la ciudad de Madrid?
select * from cliente;
select count(codigo_cliente),ciudad from cliente where ciudad like "Madrid";
-- 8.¿Calcula cuántos clientes tiene cada una de las ciudades que empiezan por M?
select count(codigo_cliente),ciudad from cliente where ciudad like "M%" group by ciudad;
-- 9.Devuelve el nombre de los representantes de ventas y el número de clientes al que atiende cada uno.
select count(codigo_cliente), empleado.nombre from empleado,cliente where empleado.codigo_empleado = cliente.codigo_empleado_rep_ventas
group by empleado.codigo_empleado;
-- 9 
select empleado.nombre, cliente.codigo_cliente from empleado
join cliente on cliente.codigo_empleado_rep_ventas=empleado.codigo_empleado;
-- 10 
select count(cliente.nombre_cliente) from cliente 
where cliente.codigo_empleado_rep_ventas is null;
-- 11
select * from cliente;
-- 12
select distinct count(producto.codigo_producto), pedido.codigo_pedido  from producto
join detalle_pedido on detalle_pedido.codigo_producto=producto.codigo_producto
join pedido on pedido.codigo_pedido=detalle_pedido.codigo_pedido
group by pedido.codigo_pedido;
-- 13 
select sum(detalle_pedido.cantidad) from detalle_pedido 
join pedido on pedido.codigo_pedido=detalle_pedido.codigo_pedido;
-- 14
select producto.nombre, sum(detalle_pedido.cantidad)  from producto
join detalle_pedido on detalle_pedido.codigo_producto=producto.codigo_producto
join pedido on pedido.codigo_pedido=detalle_pedido.codigo_pedido
group by detalle_pedido.cantidad
order by 2 desc limit 20;
-- 15
select sum(detalle_pedido.cantidad*detalle_pedido.precio_unidad), 
sum(detalle_pedido.cantidad*detalle_pedido.precio_unidad)*0.21 iva, 
(sum(detalle_pedido.cantidad*detalle_pedido.precio_unidad)+sum(detalle_pedido.cantidad*detalle_pedido.precio_unidad)*0.21)
from detalle_pedido;
-- 16
select producto.codigo_producto, (detalle_pedido.cantidad*detalle_pedido.precio_unidad), 
(detalle_pedido.cantidad*detalle_pedido.precio_unidad)*0.21 iva from detalle_pedido
join producto on producto.codigo_producto=detalle_pedido.codigo_producto;
-- 17
select producto.codigo_producto, (detalle_pedido.cantidad*detalle_pedido.precio_unidad), 
(detalle_pedido.cantidad*detalle_pedido.precio_unidad)*0.21 iva from detalle_pedido
join producto on producto.codigo_producto=detalle_pedido.codigo_producto
where producto.codigo_producto like 'or%';
-- 18
select producto.nombre, 
detalle_pedido.cantidad, 
detalle_pedido.cantidad*detalle_pedido.precio_unidad, 
(sum(detalle_pedido.cantidad*detalle_pedido.precio_unidad)+
sum(detalle_pedido.cantidad*detalle_pedido.precio_unidad)*0.21) as 'total'
from detalle_pedido
join producto on producto.codigo_producto=detalle_pedido.codigo_producto
group by producto.nombre
having total>3000;