-- 1
select * from concesionario;
-- 2
select apellido from cliente;
-- 3
select apellido, ciudad from cliente;
-- 5
select * from marca where ciudad like 'Barcelona';
-- 6
select * from cliente where ciudad like 'Madrid';
-- 7
select apellido, ciudad from cliente where ciudad like 'Madrid';
-- 8
select * from distribuyen where cantidad > 15;
-- 9
select cifc from distribuyen where cantidad > 18;
-- 10
select * from cliente where ciudad <> 'Madrid';
select * from cliente where ciudad != 'Madrid';
select * from cliente where ciudad not like 'Madrid';
-- 11
select * from cliente where apellido like 'Garcia' 
and ciudad like 'Madrid';
-- 12
select cifc from distribuyen where cantidad >10 and cantidad <5;
-- 13
select cifc from distribuyen where cantidad between 10 and 18;
-- 14
select codcoche from coche where nombre like 'c%';
-- 15
select codcoche from coche where nombre not like '%a%';
-- 16
select cifm, nombre from marca where ciudad like '_i%';
-- 17
select cifc from distribuyen where cantidad is not null;
-- 18
select nombre from marca order by nombre;