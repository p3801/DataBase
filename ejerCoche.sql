create database Ejemplo_coche;
use Ejemplo_coche;

create table cliente(
	dni int primary key,
    nombe varchar(255),
    apellido varchar(255),
    ciudad varchar(255)
);
create table coche(
	codcoche int primary key,
    nombre varchar(255),
    modelo varchar(255)
);
create table marca(
	cifm int primary key,
    nombre varchar(255),
    ciudad varchar(255)
);
create table concesionario(
	cifc int primary key,
    nombre varchar(255),
    ciudad varchar(255)
);
create table venden(
	cifc int,
    dni int,
    codcoche int,
    primary key(cifc, dni, codcoche),
    constraint id_concesionario_vende foreign key (cifc) references concesionario(cifc),
    constraint id_dni_vende foreign key (dni) references cliente(dni),
    constraint id_coche_vende foreign key (codcoche) references coche(codcoche),
    color varchar(255)
);
create table distribuyen(
	cifc int, 
    codcoche int,
    primary key (cifc, codcoche),
    constraint id_concesionario_coche foreign key (cifc) references concesionario(cifc),
    constraint id_coche_concesionario foreign key (codcoche) references coche(codcoche),
    cantidad int
);
create table fabrica(
	cifm int, 
    codcoche int,
    primary key (cifm, codcoche),
    constraint id_marca_fabrica foreign key (cifm) references marca(cifm),
    constraint id_coche_fabrica foreign key (codcoche) references coche(codcoche)
);
alter table venden add column fecha_de_venta date;
alter table concesionario add column fecha_de_venta date;
select * from concesionario;
update concesionario set fecha_de_venta = "2018-12-12" where cifc="1";
update concesionario set fecha_de_venta = "2019-12-19" where cifc=2;
update concesionario set fecha_de_venta = "2020-12-20" where cifc=3;
update concesionario set fecha_de_venta = "2021-12-21" where cifc=4;