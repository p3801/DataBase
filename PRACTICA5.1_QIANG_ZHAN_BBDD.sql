-- 1
select max(salary) as Máximo, min(salary) as Mínimo, sum(salary) as Suma, avg(salary) as Media from employees;
-- 2
select max(salary), min(salary), sum(salary), avg(salary), job_id from employees group by job_id;
-- 3
select count(employee_id), job_id from employees group by job_id;
-- 4
select count(distinct manager_id)  as "NumeroDeDirectores" from employees;
-- 5
select max(salary) - min(salary) as DIFERENCIA from employees;
-- 6
select min(salary) from employees where manager_id != 0 group by salary having salary > 6000 order by salary desc;
-- 7
select first_name as Nombre, department_id as Localizacion, count(employee_id) as Numerodeempleados, avg(salary) as Salario from employees group by department_id;
select first_name as Nombre, location_id as Localizacion, count(employee_id) as Numerodeempleados, avg(salary) as Salario from emp_details_view group by department_id;
-- 8
select count(employee_id), end_date from job_history group by year(end_date);
-- 9
select min(timestampdiff (year, start_date, end_date)),  max(timestampdiff (year, start_date, end_date)), floor(avg(timestampdiff (year, start_date, end_date))), count(timestampdiff (year, start_date, end_date)) from job_history;
-- 10
 select avg(salary), manager_id , count(employee_id)from employees where commission_pct is not null group by manager_id having count(employee_id) >=6;
