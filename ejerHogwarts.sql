create database hogwarts;
use hogwarts;

-- Ejercicio 1
create table alumno(
	id int primary key,
    nombre varchar(255) not null,
    apellido varchar(255) not null,
    fechaNacimiento date not null,
    fechaMuerte date
);
create table profesor(
	id int primary key,
    nombre varchar(255) not null,
    apellido varchar(255) not null,
    fechaNacimiento date not null,
    fechaMuerte date,
    especialidad varchar(255) check(especialidad in ("pociones","transformaciones","hechizos"))
);
create table casa(
	id int primary key,
    nombre varchar(255) not null,
    ubicacion varchar(255) not null
);
create table fantasma(
	id int primary key,
    apodo varchar(255) not null,
    nombreReal varchar(255) not null,
    apellidoReal varchar(255) not null,
    fechaMuerte date,
    causaMuerte varchar(255)
);
create table asignatura(
	id int primary key,
    nombre varchar(255) not null
);
create table curso(
	id int primary key,
    anio year not null,
    prueba varchar(255) not null
);
create table varita(
	id int primary key,
    madera varchar(255) not null,
    nucleo varchar(255) not null,
    longitud real(4,2) not null
);

-- Ejercicio 2
create table alumno_varita(
	idfk_alumno int,
    idfk_varita int,
    primary key(idfk_alumno,idfk_varita),
    foreign key (idfk_alumno) references alumno(id),
    foreign key (idfk_varita) references varita(id),
    fecha_obtencion date not null,
    fecha_perdida date
);
create table profesor_varita(
	idfk_profesor int,
    idfk_varita int,
    primary key(idfk_profesor,idfk_varita),
    foreign key (idfk_profesor) references profesor(id),
    foreign key (idfk_varita) references varita(id),
    fecha_obtencion date not null,
    fecha_perdida date
);
create table profesor_asignatura_alumno(
	idfk_profesor int,
    idfk_varita int,
    idfk_alumno int,
    primary key(idfk_profesor,idfk_varita,idfk_alumno),
    foreign key (idfk_profesor) references profesor(id),
    foreign key (idfk_varita) references varita(id),
    foreign key (idfk_alumno) references alumno(id),
    nota varchar(255),
    anio int
);
create table curso_asignatura(
	idfk_curso int,
    idfk_asignatura int,
    primary key(idfk_curso,idfk_asignatura),
    foreign key (idfk_curso) references curso(id),
    foreign key (idfk_asignatura) references asignatura(id)
);
-- esto esta mal es casa a alumno fantasma
alter table fantasma add idfk_casa int;
alter table fantasma add foreign key(idfk_casa) references casa(id);
alter table alumno add idfk_casa int;
alter table alumno add foreign key(idfk_casa) references casa(id);
alter table profesor add idfk_casa int;
alter table profesor add foreign key(idfk_casa) references casa(id);
create table profesor_asignatura(
	idfk_profesor int,
    idfk_asignatura int,
    primary key(idfk_profesor,idfk_asignatura),
    foreign key (idfk_profesor) references profesor(id),
    foreign key (idfk_asignatura) references asignatura(id),
	fecha_inicio date not null,
    fecha_fin date
);

-- Ejercicio 3
insert into casa value (1,"dumbledore","norte");
insert into casa value (2,"Slytherin","sur");
insert into alumno value (1,"Harry","Popoter",'1990/02/02',null,1);
insert into alumno value (2,"Hermione","Granger",'1990/10/10',null,2);
insert into varita value (1,"Alpino","plutonio",1.2);
insert into varita value (2,"Roble","Fenix",2.2);
insert into asignatura value (1,"pocion");
insert into asignatura value (2,"conjuro");
insert into curso value (1,1991,"hechiceria");
insert into curso value (2,1999,"alquimia");
insert into fantasma value(1,"Godofredo","Severus","Snape","1996-10-10","salvar a poter",1);
insert into fantasma value(2,"El blanco","Albus","dumbledore","1995-2-2","salvar tb a poter",2);
insert into profesor value(1,"Minerva","McGonagall","1997-12-12",null,"transformaciones",1);
insert into profesor value(2,"Severus","Snape","1982-3-3","1996-10-10","pociones",2);
insert into alumno_varita value (1,1,"1991-1-1",null);
insert into alumno_varita value (2,2,"1998-8-8",null);
insert into curso_asignatura value (1,1);
insert into curso_asignatura value (2,2);
insert into profesor_asignatura value (1,1,'1991-5-5',null);
insert into profesor_asignatura value (2,2,'2000-6-6',null);
insert into profesor_asignatura_alumno value (1,1,1,"F",1999);
insert into profesor_asignatura_alumno value (2,2,2,"A",1998);
insert into profesor_varita value (1,1,'1991-9-9',null);
insert into profesor_varita value (2,2,'1995-2-2',null);

-- Ejercicio 4
update alumno set fechaMuerte='2021/11/30' where idfk_casa=1;
update fantasma set causaMuerte="desconocida" where fantasma(fechaMuerte)<1300;
update casa set idfk_profesor=3 where casa(ubicacion)="torre";
select nombre from alumno where nombre like '%a%' and nombre like '_____' or nombre = "pepe" ;
select nombre from alumno where nombre like '%a%' and nombre like '__';
select nombre from alumno where nombre like '_____';
select nombre,id from alumno where (id<= 10 and id between 1 and 20) or id is null;
