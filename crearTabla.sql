-- crear base de datoss
create database ejemplo;
-- eliminar base de datos
drop database ejemplo;
-- usar base de dates
use ejemplo;
-- crear tabla

create table prueba2(
	id int primary key,
	nommbre varchar(255) not null,
    dni varchar(10) unique,
    tipo varchar(200) check (tipo="algo" or  tipo="otro"),
    -- todo lo que haya dentro con in
    tipo2 varchar(200) check (tipo2 in ("algo","otro")),
    numalgo int check (numalgo>10 and numalgo<100),
    -- comprendido
    numalgo2 int check (numalgo2 between 10 and 100)
);
create table pueba3(
	id int primary key,
	nommbre varchar(255) not null,
    dni varchar(10) unique,
    tipo varchar(200) check (tipo="algo" or  tipo="otro"),
    -- todo lo que haya dentro con in
    tipo2 varchar(200) check (tipo2 in ("algo","otro")),
    numalgo int check (numalgo>10 and numalgo<100),
    -- comprendido
    numalgo2 int check (numalgo2 between 10 and 100),
    idp2 int,
    -- poner nombre al registro
    constraint fk_prueba4_prueba2 foreign key (idp2) references prueba2(Id)
);

-- añadir la columna piso de tipo entero a prueba 1
alter table prueba1 add piso int;
-- borrar columna
alter table prueba1 drop column piso;
-- modificar una columna añadiendo pk a un campo
alter table prueba1 modify column id int primary key;